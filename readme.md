### Spring访问数据库

###### 使用JDBC
1. 为Configuration注入JdbcTemplate的三方Bean
2. 使用JdbcTemplate简化操作数据库

###### 使用声明式事务
1. 为Configuration注入PlatformTransactionManager三方Bean
2. 为方法或类增加@Transactional注解来声明事务
3. 在注解内指定异常的类型来自动回滚事务
4. REQUIRED是默认的事务传播级别

###### 使用DAO
DAO: Data Access Object, 数据访问的对象
1. 继承JdbcSupport类创建JdbcDaoSupport并持有jdbcTemplate
2. 创建AbstractDao泛型类并定义操作数据库通用方法
3. 创建UserDao的Bean并继承自AbstractDao,拥有父类通用操作方法
4. 在业务UserService中注入UserDao以操作数据库

##### 集成Hibernate实现ORM操作
1. 创建LocalSessionFactoryBean以自动生成SessionFactory，用于创建JDBC的session
2. 基于SessionFactory创建HibernateTransactionManager，用于配合Hibernate使用声明式事务
3. 基于SessionFactory创建HibernateTemplate，用于简化代码使用Hibernate工具类
4. 定义@Entity并使用@Column()设置字段，定义ORM的Java Bean，简称EntityBean
5. 使用hibernateTemplate下的save、delete、update分别实现增删改操作
6. 使用hibernateTemplate.findByExample()实现查询
7. 使用hibernateTemplate.findByCriteria()实现查询
8. 使用hibernateTemplate.find()实现HQL查询
9. 使用hibernateTemplate.findByNamedQuery()实现NamedQuery查询(需要提前在EntityBean中定义)
10. 使用Hibernate原生接口得到session进行查询

##### 集成JPA实现ORM操作
1. 基于LocalContainerEntityManagerFactoryBean自动生成entityManager，用于操作数据库
2. 使用entityManager下的find进行查询
3. 使用entityManager下的find进行查询
4. 使用JPA下的Criteria进行查询
5. 使用JPA的JPQL进行查询，类似HQL
6. 使用JPA下的NamedQuery进行查询

##### 集成MyBatis实现半自动ORM操作
1. 基于sqlSessionFactoryBean自动生成sqlSessionFactory
2. 创建Mapper接口，定义User与数据库表users的sql操作语句和映射方法
3. 在Config类中增加@MapperScan()的注解，说明需要扫描的Mapper接口的路径
4. 在业务类中注入UserMapper，使用UserMapper定义的方法操作数据库

