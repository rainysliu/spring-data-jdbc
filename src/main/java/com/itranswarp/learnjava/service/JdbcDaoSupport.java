package com.itranswarp.learnjava.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DaoSupport;
import org.springframework.jdbc.core.JdbcTemplate;

public abstract class JdbcDaoSupport extends DaoSupport {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public final JdbcTemplate getJdbcTemplate() {
        return this.jdbcTemplate;
    }

    public final void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
