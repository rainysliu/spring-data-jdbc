package com.itranswarp.learnjava.service;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class BonusService {
    @Transactional
    public void addBonus(long userId, int bonus) { // 事务开始
        // ...
    } // 事务结束
}
