package com.itranswarp.learnjava.service;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class UserDao extends AbstractDao<User> {
    @Override
    protected Class<User> getParameterizedType() {
        return User.class;
    }

    @Override
    protected void checkDaoConfig() throws IllegalArgumentException {

    }

}
