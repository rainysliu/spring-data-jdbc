package com.itranswarp.learnjava.entity;

import javax.persistence.*;

@NamedQueries(
        @NamedQuery(
                // 查询名称:
                name = "login",
                // 查询语句:
                query = "SELECT u FROM EntityUser u WHERE u.email=?0 AND u.password=?1"
        )
)
@Table(name = "entityusers")
@Entity
public class EntityUser extends AbstractEntity {
    @Column(nullable = false, unique = true, length = 100)
    private String email;

    @Column(nullable = false, length = 100)
    private String password;

    @Column(nullable = false, length = 100)
    private String name;

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
