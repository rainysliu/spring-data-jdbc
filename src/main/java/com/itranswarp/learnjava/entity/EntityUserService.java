package com.itranswarp.learnjava.entity;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class EntityUserService {
    @Autowired
    HibernateTemplate hibernateTemplate;

    @Autowired
    SessionFactory sessionFactory;

    @PersistenceContext
    EntityManager em;

    // Insert操作
    public EntityUser register(String email, String password, String name) {
        // 创建一个User对象:
        EntityUser user = new EntityUser();
        // 设置好各个属性:
        user.setEmail(email);
        user.setPassword(password);
        user.setName(name);
        // 不要设置id，因为使用了自增主键
        // 保存到数据库:
        hibernateTemplate.save(user);
        // 现在已经自动获得了id:
        System.out.println("新增用户ID为 " + user.getId());
        return user;
    }

    // Delete操作
    public boolean deleteUser(Long id) {
        EntityUser user = hibernateTemplate.get(EntityUser.class, id);
        if (user != null) {
            hibernateTemplate.delete(user);
            return true;
        }
        return false;
    }

    // Update操作
    public void updateUser(Long id, String name) {
        EntityUser user = hibernateTemplate.load(EntityUser.class, id);
        user.setName(name);
        hibernateTemplate.update(user);
        System.out.printf("更新用户%d的用户名为%s成功", id, name);
    }

    // 使用Example查询
    public EntityUser login(String email, String password) {
        EntityUser example = new EntityUser();
        example.setEmail(email);
        example.setPassword(password);
        List<EntityUser> list = hibernateTemplate.findByExample(example);
        return list.isEmpty() ? null : list.get(0);
    }

    // 使用Criteria查询
    public EntityUser login2(String email, String password) {
        DetachedCriteria criteria = DetachedCriteria.forClass(EntityUser.class);
        criteria.add(
                Restrictions.and(
                        Restrictions.or(
                                Restrictions.eq("email", email),
                                Restrictions.eq("name", email)
                        ),
                        Restrictions.eq("password", password)
                )
        );
        List<EntityUser> list = (List<EntityUser>) hibernateTemplate.findByCriteria(criteria);
        return list.isEmpty() ? null : list.get(0);
    }

    // 使用HQL查询
    public EntityUser login3(String email, String password) {
        List<EntityUser> list = (List<EntityUser>) hibernateTemplate.find("FROM EntityUser WHERE email=?0 AND password=?1", new Object[] {email, password});

        return list.isEmpty() ? null : list.get(0);
    }

    // 使用NamedQuery
    public EntityUser login4(String email, String password) {
        List<EntityUser> list = (List<EntityUser>) hibernateTemplate.findByNamedQuery("login", email, password);
        return list.isEmpty() ? null : list.get(0);
    }

    // 使用Hibernate原生接口
    public void operation() {
        Session session = null;
        boolean isNew = false;
        // 获取当前Session或者打开新的Session:
        try {
            session = this.sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = this.sessionFactory.openSession();
            isNew = true;
        }
        // 操作Session:
        try {
            EntityUser user = session.load(EntityUser.class, 1L);
            System.out.println("加载user:" + user.getName());
        }
        finally {
            // 关闭新打开的Session:
            if (isNew) {
                session.close();
            }
        }
    }

    // 使用JPA的find进行查询
    public EntityUser getUserById(long id) {
        EntityUser user = this.em.find(EntityUser.class, id);
        if (user == null) {
            throw new RuntimeException("User not found by id: " + id);
        }
        return user;
    }

    // 使用JPA的Criteria查询：
    public EntityUser fetchUserByEmail(String email) {
        // CriteriaBuilder:
        var cb = em.getCriteriaBuilder();
        CriteriaQuery<EntityUser> q = cb.createQuery(EntityUser.class);
        Root<EntityUser> r = q.from(EntityUser.class);
        q.where(cb.equal(r.get("email"), cb.parameter(String.class, "e")));
        TypedQuery<EntityUser> query = em.createQuery(q);
        // 绑定参数:
        query.setParameter("e", email);
        // 执行查询:
        List<EntityUser> list = query.getResultList();
        return list.isEmpty() ? null : list.get(0);
    }

    // 使用JPA的JPQL查询，类似HQL
    public EntityUser getUserByEmail(String email) {
        // JPQL查询:
        TypedQuery<EntityUser> query = em.createQuery("SELECT u FROM User u WHERE u.email = :e", EntityUser.class);
        query.setParameter("e", email);
        List<EntityUser> list = query.getResultList();
        if (list.isEmpty()) {
            throw new RuntimeException("User not found by email.");
        }
        return list.get(0);
    }

    //JPA也支持NamedQuery，同样需要在Entity类上增加NamedQueries
    public EntityUser login5(String email, String password) {
        TypedQuery<EntityUser> query = em.createNamedQuery("login", EntityUser.class);
        query.setParameter("e", email);
        query.setParameter("p", password);
        List<EntityUser> list = query.getResultList();
        return list.isEmpty() ? null : list.get(0);
    }
}
