package com.itranswarp.learnjava.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Book extends AbstractEntity {
    @Column(nullable = false, length = 100)
    private String title;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
