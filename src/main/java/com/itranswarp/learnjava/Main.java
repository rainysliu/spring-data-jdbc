package com.itranswarp.learnjava;

import com.itranswarp.learnjava.entity.EntityUser;
import com.itranswarp.learnjava.entity.EntityUserService;
import com.itranswarp.learnjava.service.User;
import com.itranswarp.learnjava.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        UserService userService = context.getBean(UserService.class);
        User user;
        // 使用JdbcTemplate操作数据库
        user = userService.register("ang@qq.com", "123456", "ang");
        System.out.println("User:" + user.getName() + " have registered!");
        user = userService.getUserById(0);
        System.out.println("User:" + user.getId() + " get success!");
        user = userService.getUserByName("ang");
        System.out.println("User:" + user.getName() + " get success!");
        user = userService.getUserByEmail("ang@qq.com");
        System.out.println("User:" + user.getEmail() + " get success!");
        long count = userService.getUsersCount();
        System.out.println("Users count: " + count);
        List<User> users = userService.getUsers(1);
        System.out.println("Get users: " + Arrays.toString(users.stream().map(User::getName).toArray()));
        userService.updateUser(new User(0, "ang@qq.com", "234567", "ang1"));
        user = userService.getUserById(0);
        System.out.println("User:" + user.getName() + " get success!");

        // 使用MyBatis进行半自动ORM操作数据库
        user = userService.getUserById2(0);
        System.out.println("User:" + user.getName() + " get success!");

        // 使用DAO操作数据库
        userService.getAll(0);
        userService.getById(0);
        userService.deleteUserByID(0);

        // 使用Hibernate进行ORM操作数据库
        EntityUserService entityUserService = context.getBean(EntityUserService.class);
        EntityUser entityUser = entityUserService.register("meng@qq.com", "123456", "meng");
        System.out.println(entityUser.getName() + " have registered!");
        entityUserService.updateUser(1L, "meng1");
        EntityUser entityUser1 = entityUserService.login("ew@qq.com", "");
        System.out.println(entityUser1 != null ? entityUser1.getName() + "登陆成功！" : "登陆失败！");
        EntityUser entityUser2 = entityUserService.login2("meng@qq.com", "123456");
        System.out.println(entityUser1 != null ? entityUser2.getName() + "登陆成功！" : "登陆失败！");
        EntityUser entityUser3 = entityUserService.login3("ew@qq.com", "登陆失败！");
        System.out.println(entityUser1 != null ? entityUser3.getName() + "登陆成功！" : "登陆失败！");
        EntityUser entityUser4 = entityUserService.login4("meng@qq.com", "123456");
        System.out.println(entityUser1 != null ? entityUser4.getName() + "登陆成功！" : "登陆失败！");
        entityUserService.operation();
        boolean res = entityUserService.deleteUser(1L);
        System.out.println("User: %d delete ".formatted(1) + (res? "successful!": "failed!"));

        // 使用JAP进行ORM操作数据库
        entityUserService.getUserById(0);
        entityUserService.getUserByEmail("meng@qq.com");
        entityUserService.fetchUserByEmail("meng@qq.com");
        entityUserService.login5("meng@qq.com", "123456");
    }
}
